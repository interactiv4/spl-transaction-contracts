<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Transaction\Api\Exception;

use Exception;

/**
 * Class TransactionException.
 *
 * All exceptions raised by transaction interfaces MUST be either:
 * - Subclasses of this class
 * - @see \RuntimeException or its subclasses
 *
 * For @see \LogicException or more specific exceptions, wrap them as previous into appropriate exception type:
 *
 * @see BeginTransactionException
 * @see RollbackTransactionException
 * @see CommitTransactionException
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Transaction
 */
abstract class TransactionException extends Exception
{
}
