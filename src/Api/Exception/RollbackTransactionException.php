<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Transaction\Api\Exception;

/**
 * Class RollbackTransactionException.
 *
 * Thrown when a non-runtime error occurs while performing rollback transaction.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Transaction
 */
final class RollbackTransactionException extends TransactionException
{
}
