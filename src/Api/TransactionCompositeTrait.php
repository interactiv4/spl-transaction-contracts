<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Transaction\Api;

use Interactiv4\Contracts\SPL\Exception\Wrapper\Api\ExceptionWrapperTrait;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\BeginTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\CommitTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\RollbackTransactionException;

/**
 * Trait TransactionCompositeTrait.
 *
 * Use this trait to help yourself to implement TransactionInterface as composite.
 *
 * @see TransactionInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Transaction
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
trait TransactionCompositeTrait
{
    use TransactionTrait;
    use ExceptionWrapperTrait;

    /**
     * @var TransactionInterface[]
     */
    protected $transactions = [];

    /**
     * @var TransactionInterface[]
     */
    protected $initializedTransactions = [];

    /**
     * Try to begin all transactions before running custom logic.
     * If any of them fails to begin, stop and rollback begun transactions.
     *
     * {@inheritdoc}
     */
    public function doBeginTransaction(array $context = []): void
    {
        $exceptions = [];
        $this->initializedTransactions = [];

        foreach ($this->transactions as $transaction) {
            try {
                $transaction->beginTransaction($context);
                $this->initializedTransactions[] = $transaction;
            } catch (BeginTransactionException $e) {
                // Stop beginning further transactions, store exception for later wrapping
                $exceptions[] = $e;

                try {
                    // Rollback all already initialized transactions
                    $this->rollback($context);
                } catch (RollbackTransactionException $e) {
                    // Store rollback exception for later wrapping
                    $exceptions[] = $e;
                }

                break;
            }
        }

        if (\count($exceptions)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(BeginTransactionException::class, ...$exceptions);
        }
    }

    /**
     * At this point, custom logic has failed, so all transactions MUST have the chance to rollback themselves.
     *
     * {@inheritdoc}
     */
    public function doRollback(
        array $previousTransactionState,
        array $context = []
    ): void {
        $exceptions = [];

        // Rollback in reverse order, inside out
        while ($transaction = \array_pop($this->initializedTransactions)) {
            try {
                $transaction->rollback($context);
            } catch (RollbackTransactionException $e) {
                // Store for later wrapping, allow rollback of all initialized transactions
                $exceptions[] = $e;
            }
        }

        if (\count($exceptions)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(RollbackTransactionException::class, ...$exceptions);
        }
    }

    /**
     * At this point, custom logic has succeeded, so all transactions MUST have the chance to commit themselves.
     *
     * {@inheritdoc}
     */
    public function doCommit(
        array $previousTransactionState,
        array $context = []
    ): void {
        $exceptions = [];

        while ($transaction = \array_pop($this->initializedTransactions)) {
            try {
                $transaction->commit($context);
            } catch (CommitTransactionException $e) {
                $exceptions[] = $e;

                try {
                    // Try rollback for non-committed transaction, to avoid inconsistent transaction state
                    $transaction->rollback($context);
                } catch (RollbackTransactionException $e) {
                    // Store for later wrapping, allow rollback of all initialized transactions
                    $exceptions[] = $e;
                }
            }
        }

        if (\count($exceptions)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(CommitTransactionException::class, ...$exceptions);
        }
    }
}
