<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Transaction\Api;

use Interactiv4\Contracts\SPL\Transaction\Api\Exception\BeginTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\CommitTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\RollbackTransactionException;
use RuntimeException;

/**
 * Interface TransactionInterface.
 *
 * Implement transactional logic by implementing this interface.
 *
 * @see TransactionTrait
 * @see TransactionCompositeTrait
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Transaction
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface TransactionInterface
{
    /**
     * Run custom logic before transaction is processed. It can be used to store current application state.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param array $context Optional additional data.
     *
     * @return void
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws BeginTransactionException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function beginTransaction(array $context = []): void;

    /**
     * Run custom logic when transaction has failed. It can be used to restore current application state.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param array $context Optional additional data.
     *
     * @return void
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws RollbackTransactionException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function rollback(array $context = []): void;

    /**
     * Run custom logic when transaction has succeeded.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param array $context Optional additional data.
     *
     * @return void
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws CommitTransactionException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function commit(array $context = []): void;
}
