<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Transaction\Api;

use Interactiv4\Contracts\SPL\Transaction\Api\Exception\BeginTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\CommitTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\RollbackTransactionException;
use RuntimeException;

/**
 * Trait TransactionTrait.
 *
 * Useful trait for implementing common aspects of TransactionInterface handling.
 * It may be useful to be included and overwrite only required methods.
 *
 * @see TransactionInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Transaction
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
trait TransactionTrait
{
    /**
     * @var array
     */
    protected $transactionStates = [];

    /**
     * {@inheritdoc}
     */
    public function beginTransaction(array $context = []): void
    {
        $this->transactionStates[] = $this->getCurrentState($context);
        $this->doBeginTransaction($context);
    }

    /**
     * Custom logic for beginning transaction.
     * Overwrite this method in actual class if needed.
     *
     * @param array $context Optional additional data.
     *
     * @return void
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws BeginTransactionException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    abstract protected function doBeginTransaction(array $context = []): void;

    /**
     * {@inheritdoc}
     */
    public function rollback(array $context = []): void
    {
        $previousTransactionState = \array_pop($this->transactionStates);
        $this->doRollback($previousTransactionState, $context);
    }

    /**
     * Custom logic for rollback transaction.
     * Overwrite this method in actual class.
     *
     * @param array $previousTransactionState Should accept one of the sets of data returned by getTransactionState method.
     * @param array $context Optional additional data.
     *
     * @return void
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws RollbackTransactionException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    abstract protected function doRollback(
        array $previousTransactionState,
        array $context = []
    ): void;

    /**
     * {@inheritdoc}
     */
    public function commit(array $context = []): void
    {
        $previousTransactionState = \array_pop($this->transactionStates);
        $this->doCommit($previousTransactionState, $context);
    }

    /**
     * Custom logic for commit transaction.
     * Overwrite this method in actual class.
     *
     * @param array $previousTransactionState Should accept one of the sets of data returned by getTransactionState method.
     * @param array $context Optional additional data.
     *
     * @return void
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws CommitTransactionException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    abstract protected function doCommit(
        array $previousTransactionState,
        array $context = []
    ): void;

    /**
     * Get current state related to transaction, i.e. data needed for rollback if needed.
     * Overwrite this method in actual class if needed.
     *
     * @param array $context Optional additional data.
     *
     * @return array Actual state to be stored, if any.
     */
    protected function getCurrentState(array $context = []): array
    {
        return [];
    }
}
