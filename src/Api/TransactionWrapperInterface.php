<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Transaction\Api;

use Closure;
use Exception;

/**
 * Interface TransactionWrapperInterface.
 *
 * Run a callback wrapped within a transaction.
 *
 * @see TransactionWrapperTrait
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Transaction
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface TransactionWrapperInterface
{
    /**
     * Wrap callback execution into provided transaction.
     * Return whatever the callable returns. It may throw any kind of exception.
     *
     * @param TransactionInterface $transaction
     * @param array                $transactionContext Optional, additional arbitrary data supplied to transaction.
     * @param Closure              $callback
     * @param mixed                ...$args Arguments supplied to callback
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function execute(
        TransactionInterface $transaction,
        array $transactionContext,
        Closure $callback,
        ...$args
    );
}
