<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Transaction\Api;

use Closure;
use Exception;
use Interactiv4\Contracts\SPL\Exception\Wrapper\Api\ExceptionWrapperTrait;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\BeginTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\CommitTransactionException;
use Interactiv4\Contracts\SPL\Transaction\Api\Exception\RollbackTransactionException;

/**
 * Trait TransactionWrapperTrait.
 *
 * Use this trait to help yourself to implement TransactionWrapperInterface.
 *
 * @see TransactionWrapperInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Transaction
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
trait TransactionWrapperTrait
{
    use ExceptionWrapperTrait;

    /**
     * {@inheritdoc}
     */
    public function execute(
        TransactionInterface $transaction,
        array $transactionContext,
        Closure $callback,
        ...$args
    ) {
        $exceptions = [];

        try {
            $transaction->beginTransaction($transactionContext);
        } catch (BeginTransactionException $e) {
            $exceptions[] = $e;
        } catch (Exception $e) {
            $exceptions[] = $e;

            try {
                $transaction->rollback($transactionContext);
            } catch (RollbackTransactionException $e) {
                $exceptions[] = $e;
            }
        }

        if (\count($exceptions)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(BeginTransactionException::class, ...$exceptions);
        }

        try {
            $result = $callback(...$args);
        } catch (Exception $e) {
            $exceptions[] = $e;

            try {
                $transaction->rollback($transactionContext);
            } catch (RollbackTransactionException $e) {
                $exceptions[] = $e;
            }

            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(RollbackTransactionException::class, ...$exceptions);
        }

        try {
            $transaction->commit($transactionContext);
        } catch (CommitTransactionException $e) {
            $exceptions[] = $e;
        }

        if (\count($exceptions)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(CommitTransactionException::class, ...$exceptions);
        }

        /** @noinspection PhpUndefinedVariableInspection */
        return $result;
    }
}
